" Vundle
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'dpelle/vim-LanguageTool'
Plugin 'dpelle/vim-Grammalecte'
Plugin 'vim-scripts/a.vim'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/nerdtree'
Plugin 'airblade/vim-gitgutter'
Plugin 'vim-scripts/cscope.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" cscope settings
cs add $HOME/.vim/cscope.out
set cscopeverbose
set cscopequickfix=s-,c-,d-,i-,t-,e-,g-
set cscopetag

" Show line number
set nu

" Show list characters
set list

" Use dot for trailing spaces.
set showbreak=\\
set listchars=tab:>>,trail:.,extends:>,precedes:<,nbsp:~

" Set encoding to UTF-8
set encoding=utf-8

" Persistent Undo
set undofile
set undodir=~/.vim/undodir

" Tabs and spaces
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set smartindent
set bs=2
set ruler

match ErrorMsg '\%>65v.\+'


" Remeber the last position of the cursor
" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.vim/data/viminfo
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

let g:ycm_autoclose_preview_window_after_insertion = 1

" Open CtrlP result in a separate tab
let g:ctrlp_prompt_mappings = {
    \ 'AcceptSelection("e")': ['<2-LeftMouse>'],
    \ 'AcceptSelection("t")': ['<cr>'],
    \ }

" Use syntax coloring
syntax on
" colorscheme sublimemonokai
colorscheme monokai
" colorscheme rastafari

set tags=tags

" Shortcuts
command! EE e %:p:h
command! TEE tabe %:p:h
command! TE tabe
command! ReplaceTabsWithSpaces %s/\t/  /g

map <F3> :TEE<CR>
map <F9> :YcmDiags<CR>
map ; :CtrlP<CR>
map <C-e> :NERDTreeToggle<CR>

nnoremap <leader>w <C-w>

" cscope.vim key mappings
nnoremap <leader>fa :call cscope#findInteractive(expand('<cword>'))<CR>
nnoremap <leader>l :call ToggleLocationList()<CR>

" s: Find this C symbol
nnoremap  <leader>fs :call cscope#find('s', expand('<cword>'))<CR>
" g: Find this definition
nnoremap  <leader>fg :call cscope#find('g', expand('<cword>'))<CR>
" d: Find functions called by this function
nnoremap  <leader>fd :call cscope#find('d', expand('<cword>'))<CR>
" c: Find functions calling this function
nnoremap  <leader>fc :call cscope#find('c', expand('<cword>'))<CR>
" t: Find this text string
nnoremap  <leader>ft :call cscope#find('t', expand('<cword>'))<CR>
" e: Find this egrep pattern
nnoremap  <leader>fe :call cscope#find('e', expand('<cword>'))<CR>
" f: Find this file
nnoremap  <leader>ff :call cscope#find('f', expand('<cword>'))<CR>
" i: Find files #including this file
nnoremap  <leader>fi :call cscope#find('i', expand('<cword>'))<CR>

let g:NERDTreeNodeDelimiter = "\u00a0"
let g:TerminusFocusReporting=0

" Remove trailing spaces upon save
" autocmd BufWritePre * :%s/\s\+$//e

command -nargs=1 Mg vimgrep <f-args> **/*.h **/*.cpp **/*.java **/*.html **/*.js **/*.py | cw

" Swap files
:set directory=$HOME/.vim/tmp//
