git clone https://github.com/vim/vim .tmp
cd .tmp
./configure --enable-cscope \
            --enable-multibyte \
            --with-features=huge \
            --enable-pythoninterp=yes \
            --enable-rubyinterp=yes \
            --enable-luainterp=yes \
            --prefix=/usr/local \
            && make \
            && sudo make install
rm -rf .tmp
