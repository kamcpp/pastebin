#!/bin/bash -x
ln -sf $PWD/vimrc ~/.vimrc
mkdir -p ~/.vim
mkdir -p ~/.vim/tmp
ln -sf $PWD/colors ~/.vim
mkdir -p ~/.vim/bundle
pushd ~/.vim/bundle
git clone https://github.com/VundleVim/Vundle.vim
popd


